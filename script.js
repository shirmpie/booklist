$(document).ready(function () {
  $("div.dropdown-item").click(function () {
    let data = $(this).text();
    $("span#data").text("Book List Year : " + data);
    let myUrl = "https://gatesbooks-api.uw.r.appspot.com/" + data;
    $('input#my-url').val(myUrl);
    $('ul.list-group li').each(function() {
        $('ul.list-group li').remove();
        });
    $.ajax({
      type: "GET",
      dataType: "json",
      url: myUrl,
      success: function (result) {
        console.log("Success!");
        parseResult(result);
      },
      error: function (e) {
        console.log("Error!");
        console.log(e);
      }
    });
  });
});

function parseResult(j) {
  var js = JSON.stringify(j);
  var jp = JSON.parse(js);
  const N = jp.books.length;
  for (var i = 0; i < N; i++) {
    var str = "<li>Author: " + jp.books[i].author + "</li>";
    $('ul.list-group').append(str);
    $('ul.list-group li:last').attr('class', 'list-group-item');
    var str = "<li>Title: " + jp.books[i].title + "</li>";
    $('ul.list-group').append(str);
    $('ul.list-group li:last').attr('class', 'list-group-item');
    var str = "<li>Abstract: " + jp.books[i].abstract + "</li>";
    $('ul.list-group').append(str);
    $('ul.list-group li:last').attr('class', 'list-group-item');
    var str = "<li>Image: <img id='pic' src='" + jp.books[i].image + "'></li>";
    $('ul.list-group').append(str);
    $('ul.list-group li:last').attr('class', 'list-group-item');
   }
}